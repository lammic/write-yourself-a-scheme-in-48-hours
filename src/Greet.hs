module Main where
import System.Environment

main :: IO ()
main = do
  putStrLn "gimme your name: "
  name <- getLine
  putStrLn $ "your name is: " ++ name
